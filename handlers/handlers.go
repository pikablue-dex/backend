package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/cighy/pikablue/backend/models"
)

var db = models.DB()

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello!")
}

func GetAll(w http.ResponseWriter, r *http.Request) {
	var pokemons models.Pokemons
	res := db.Find(&pokemons)
	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(res); err != nil {
		panic(err)
	}

}
