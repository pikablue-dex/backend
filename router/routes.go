package router

import (
	"net/http"

	"gitlab.com/cighy/pikablue/backend/handlers"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		handlers.Index,
	},
	Route{
		"GetPokemon",
		"GET",
		"/api/pokemon",
		handlers.GetAll,
	},
}
