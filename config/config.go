package config

// server config
const Port = "8080"
const Localhost = "localhost"

// db config
const DB_TYPE = "sqlite3"
const DB_LOCATION = "storage.db"
