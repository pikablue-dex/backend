package main

import (
	"log"
	"net/http"

	"gitlab.com/cighy/pikablue/backend/config"
	"gitlab.com/cighy/pikablue/backend/router"
)

func main() {
	port := ":" + config.Port
	router := router.NewRouter()

	log.Fatal(http.ListenAndServe(port, router))
}
