package models

type Pokemon struct {
	ID    uint `gorm:"primary_key"`
	DexID uint
	Name  string
}

type Pokemons []Pokemon
