package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/cighy/pikablue/backend/config"
)

type Base struct {
	gorm.Model
}

func DB() *gorm.DB {
	db, err := gorm.Open(config.DB_TYPE, config.DB_LOCATION)

	if err != nil {
		panic("failed to connect database")
	}

	return db
}
